$(document).ready(function(){

    // 내비게이션
    monavbtn = $(".moNavBtn");
    monavclosebtn = $(".moNavCloseBtn");
    header = $("header");
    subnav = $(".subNav");
    wrap = $("#wrap");
    body = $("body");
    
    monavbtn.click( function(e) {
        e.preventDefault();
        subnav.addClass("active");
        wrap.addClass("active");
        body.addClass("active");
    });

    monavclosebtn.click( function(e) {
        e.preventDefault();
        subnav.removeClass("active");
        wrap.removeClass("active");
        body.removeClass("active");
    });
    
    if($(window).width() > 1109){
        subnav.removeClass("active").hide();
        wrap.removeClass("active");
        body.removeClass("active");

        $("header nav .listWrap").mouseover(function(e){
            e.preventDefault();
            header.addClass("active");
            subnav.show();
        })
        subnav.mouseleave(function(e){
            e.preventDefault();
            header.removeClass("active");
            subnav.hide();
        })
    } else {
        subnav.unbind("mouseleave").show();
    }
    $(window).resize(function(){
        if($(window).width() > 1109){
            subnav.removeClass("active").hide();
            wrap.removeClass("active");
            body.removeClass("active");
    
            $("header nav .listWrap").mouseover(function(e){
                e.preventDefault();
                header.addClass("active");
                subnav.show();
            })
            subnav.mouseleave(function(e){
                e.preventDefault();
                header.removeClass("active");
                subnav.hide();
            })
        } else {
            header.removeClass("active");
            subnav.unbind("mouseleave").show();
        }
    });
    
    // 팝업
    $(".popup").hide();
    
    $(".popupOpen").click(function(){
        $("body").addClass("fixed");
        $(".popup").show();
        $("iframe").attr("src", "https://www.youtube.com/embed/t67iXC8vtNA?&autoplay=1&vq=highres");
    });
    $(".popupClose").click(function(){
        $("body").removeClass("fixed");
        $(".popup").hide();  
        $("iframe").attr("src", "");
    });

    //form
    $('input[type=text]').blur(function(e) {
        e.preventDefault();
        if( $(this).val().length > 0 ) {
            $(this).css("border-color","#ed008c");
        } else {
            $(this).css("border-color","#ccc");
        }
    });

})